package com.hexaware.restaurantmgmt.service;

import com.hexaware.restaurantmgmt.dao.OrderDAO;
import com.hexaware.restaurantmgmt.dao.OrderDAOImpl;
import com.hexaware.restaurantmgmt.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDAO orderDAO;


    @Override
    public Order saveOrder(Order order) {
        return orderDAO.save(order);
    }

    @Override
    public Set<Order> fetchAllOrders() {
        return this.orderDAO.fetchAll();
    }

    @Override
    public Order fetchOrderById(long orderId) {
        return this.orderDAO.fechOrderById(orderId);
    }

    @Override
    public void deleteOrderById(long orderId) {
        this.orderDAO.deleteOrder(orderId);
    }
}