package com.hexaware.restaurantmgmt.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of = "dishId")
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Dish {

    @JsonProperty("id")
    private long dishId;

    @JsonProperty("price")
    private double price;

    @JsonProperty("calories")
    private double calories;
}